package resolver

import "bitbucket.org/phm-eng/pahamify.boilerplate.go/internal/app/model/userattribution"

type UserAttributionResolver struct {
	userAttribution userattribution.UserAttribution
}

func (u UserAttributionResolver) MediaSource() *string {
	return &u.userAttribution.MediaSource
}
//
//func (u UserAttributionResolver) Campaign() *string {
//	return &u.userAttribution.Campaign
//}
//
//func (u UserAttributionResolver) AfAd() *string {
//	return &u.userAttribution.AfAd
//}
//
//func (u UserAttributionResolver) AfAdSet() *string {
//	return &u.userAttribution.AfAdset
//}
//
//func (u UserAttributionResolver) AfAdType() *string {
//	return &u.userAttribution.AfAdType
//}
//
//func (u UserAttributionResolver) AfCampaign() *string {
//	return &u.userAttribution.AfCampaign
//}
//
//func (u UserAttributionResolver) AfSiteid() *string  {
//	return &u.userAttribution.AfSiteid
//}
//
//func (u UserAttributionResolver) AfChannel() *string  {
//	return &u.userAttribution.AfChannel
//}
//
//func (u UserAttributionResolver) AfCostCurrency() *string {
//	return &u.userAttribution.AfCostCurrency
//}
//
//func (u UserAttributionResolver) AfCostValue() *string  {
//	return &u.userAttribution.AfCostValue
//}
//
//func (u UserAttributionResolver) AfCostModel() *string  {
//	return &u.userAttribution.AfCostModel
//}
//
//func (u UserAttributionResolver) AfReengagementWindow() *string  {
//	return &u.userAttribution.AfReengagementWindow
//}
//
//func (u UserAttributionResolver) EventRevenue() *string  {
//	return &u.userAttribution.EventRevenue
//}
//
//func (u UserAttributionResolver) HttpReferrer() *string  {
//	return &u.userAttribution.HttpReferrer
//}
//
//func (u UserAttributionResolver) AttributedTouchType() *string  {
//	return &u.userAttribution.AttributedTouchType
//}



