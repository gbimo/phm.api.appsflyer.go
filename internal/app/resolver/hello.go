package resolver

import (
	"context"
	"fmt"

	"bitbucket.org/phm-eng/pahamify.boilerplate.go/internal/app/model/hello"
)

// Hello return hello world text
func (r *Resolver) Hello(ctx context.Context, args struct{ Text *string }) *HelloResolver {
	result := hello.Hello{
		Text: "Hello world",
	}

	if args.Text != nil {
		result.Text = fmt.Sprintf("Hello %s", *args.Text)
	}

	return &HelloResolver{result}
}
