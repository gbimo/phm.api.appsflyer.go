package resolver

import (
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/internal/app/model/userresult"
)

// UserResultResolver contain methods for resolving UserResult model to graphql query
type UserResultResolver struct {
	userResult userresult.UserResult
}

// ID resolve id query
func (h UserResultResolver) ID() *int32 {
	id := h.userResult.UserID
	return &id
}

// UtbkRegNo resolve utbkRegNo query
func (h UserResultResolver) UtbkRegNo() *string {
	return &h.userResult.UtbkRegNo
}
