package resolver

import (
	"context"
	"errors"

	"bitbucket.org/phm-eng/pahamify.boilerplate.go/internal/app/model/userresult"
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/pkg/database"
)

// GetProfileUtbk return user result based on id
func (r *Resolver) GetProfileUtbk(ctx context.Context, args struct{ UserID int32 }) (*UserResultResolver, error) {
	usr := userresult.UserResult{}
	db := database.GetConnection()
	if err := db.Where("UserId = ?", args.UserID).First(&usr).Error; err != nil {
		return nil, errors.New("Not Found")
	}

	return &UserResultResolver{usr}, nil
}

// SetProfileUtbk set user result
func (r *Resolver) SetProfileUtbk(ctx context.Context, args struct {
	UserID    int32
	UtbkRegNo string
}) (*UserResultResolver, error) {
	usr := &userresult.UserResult{}
	db := database.GetConnection()
	var err error

	db.Where("UserId = ?", args.UserID).First(usr)

	usr.UserID = args.UserID
	usr.UtbkRegNo = args.UtbkRegNo

	if db.NewRecord(usr) {
		err = db.Create(usr).Error
	} else {
		err = db.Save(usr).Error
	}

	if err != nil {
		return nil, err
	}

	return &UserResultResolver{*usr}, nil
}
