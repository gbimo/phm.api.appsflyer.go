package resolver

import (
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/internal/app/model/userattribution"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)


func (r *Resolver) SetUserAttribute(ctx context.Context, args struct {
	MediaSource string
	//Campaign *string
	//AfAd *string
	//AfAdset string
	//AfAdType *string
	//AfCampaign *string
	//AfChannel *string
	//AfCostCurrency *string
	//AfCostModel *string
	//AfCostValue *string
	//AfReengagementWindow *string
	//AfSiteid *string
	//AttributedTouchType *string
	//HttpReferrer *string
	//EventRevenue *string
}) *UserAttributionResolver {
	//result := userattribution.UserAttribution{
	//	MediaSource: "null",
	//	Campaign: "null",
	//	AfAd: "null",
	//	AfAdset: "null",
	//	AfAdType: "null",
	//	AfCampaign: "null",
	//	AfChannel: "null",
	//	AfCostCurrency: "null",
	//	AfCostModel: "null",
	//	AfCostValue: "null",
	//	AfReengagementWindow: "null",
	//	AfSiteid: "null",
	//	AttributedTouchType: "null",
	//	HttpReferrer: "null",
	//	EventRevenue: "null",
	//}

	userAttribute := userattribution.UserAttribution{}
	res := struct {
		Data struct{
			UserAttribute struct{
				MediaSource string `json:"MediaSource"`
			} `json:"userAttribute"`
		} `json:"data"`
	}{}

	graphqlQuery := fmt.Sprintf(`{ "query": "mutation { setUserAttribute(MediaSource:\"%s\") }" }`, args.MediaSource)

	response, err := http.Post("http://localhost:8070/graphql", "application/json", bytes.NewBuffer([]byte(graphqlQuery)))


	if err == nil {
		err = json.NewDecoder(response.Body).Decode(&res)
	}

	return &UserAttributionResolver{userAttribute}
}

//func (r *Resolver) GetUserAttribute(ctx context.Context, args struct{
//	MediaSource *string
//	Campaign *string
//	AfAd *string
//	AfAdset *string
//	AfAdType *string
//	AfCampaign *string
//	AfChannel *string
//	AfCostCurrency *string
//	AfCostModel *string
//	AfCostValue *string
//	AfReengagementWindow *string
//	AfSiteid *string
//	AttributedTouchType *string
//	HttpReferrer *string
//	EventRevenue *string
//}) *UserAttributionResolver {
//	result := userattribution.UserAttribution{
//		MediaSource: "null",
//		Campaign: "null",
//		AfAd: "null",
//		AfAdset: "null",
//		AfAdType: "null",
//		AfCampaign: "null",
//		AfChannel: "null",
//		AfCostCurrency: "null",
//		AfCostModel: "null",
//		AfCostValue: "null",
//		AfReengagementWindow: "null",
//		AfSiteid: "null",
//		AttributedTouchType: "null",
//		HttpReferrer: "null",
//		EventRevenue: "null",
//	}
//
//	return &UserAttributionResolver{result}
//}
