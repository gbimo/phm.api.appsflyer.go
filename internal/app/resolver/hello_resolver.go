package resolver

import (
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/internal/app/model/hello"
)

// HelloResolver contain methods for resolving Hello model to graphql query
type HelloResolver struct {
	hello hello.Hello
}

// Text resolve text query
func (h *HelloResolver) Text() *string {
	return &h.hello.Text
}
