// Code generated for package schema by go-bindata DO NOT EDIT. (@generated)
// sources:
// mutation.graphql
// query.graphql
// schema.graphql
// type/message.graphql
// type/userattribute.graphql
// type/userresult.graphql
package schema

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func bindataRead(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	clErr := gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}
	if clErr != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes []byte
	info  os.FileInfo
}

type bindataFileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
}

// Name return file name
func (fi bindataFileInfo) Name() string {
	return fi.name
}

// Size return file size
func (fi bindataFileInfo) Size() int64 {
	return fi.size
}

// Mode return file mode
func (fi bindataFileInfo) Mode() os.FileMode {
	return fi.mode
}

// Mode return file modify time
func (fi bindataFileInfo) ModTime() time.Time {
	return fi.modTime
}

// IsDir return file whether a directory
func (fi bindataFileInfo) IsDir() bool {
	return fi.mode&os.ModeDir != 0
}

// Sys return file is sys mode
func (fi bindataFileInfo) Sys() interface{} {
	return nil
}

var _mutationGraphql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x74\x91\xc1\x6e\xe2\x40\x10\x44\xef\xf3\x15\x8d\xb8\xec\xfe\x82\x6f\x2c\xbb\xda\xa0\x88\x28\x32\x90\x9c\xc7\x9e\xb2\x19\x61\xf7\x58\x3d\x3d\x20\x2b\xca\xbf\x47\xe0\x40\x88\x21\x47\xbf\xaa\xea\xae\xf6\x68\xdf\x81\x96\x49\xad\xfa\xc0\xf4\x66\x88\xa6\xb4\xe9\x9c\x55\xd0\x66\xfd\xe7\x91\x9e\x25\x54\xbe\x81\x21\x8a\xd0\xcf\x8f\x8d\x16\xbb\x5f\x86\xe8\xe4\x8d\x10\xf2\x8e\x2a\x09\x2d\xfd\xb7\x8a\x83\xed\x29\x42\xf6\xbe\xc4\xc9\x92\x22\x64\xe1\x32\x5a\xb0\x4e\xcc\x39\x74\x9c\x2c\xa8\x7d\x54\x19\x16\x73\x6a\x0b\xc8\x10\xd0\x62\x97\xa3\x7e\x0a\x19\xad\x54\x3c\xd7\x13\x43\xf4\x3b\x3b\x6d\xfa\x0b\xb5\xbe\x31\x43\x9b\x23\x98\xa9\x8a\x2f\x92\x62\xe8\xb3\x84\xf3\x76\x15\x92\x94\xf8\x4a\x4f\x8f\xca\xdc\xb6\x9d\xf5\x35\x9f\xf1\x40\x67\xd5\xcc\xdd\x92\x08\xbd\x85\xeb\xbe\x43\x46\x23\xfc\xd3\xd4\xf9\xd6\x32\xa3\xb9\xc1\x21\xea\x3c\x89\x80\xcb\xfe\x9e\xb6\x0c\xee\x7e\xe8\xc5\x36\x09\x63\x21\x07\xb8\xb6\x35\x5a\xb0\xbe\x7a\x76\xe1\x30\x76\xac\xbc\xc2\x8f\x0f\x3c\xff\x31\xb7\x0e\xa9\xdc\x0e\x67\x5d\x1b\x1e\x54\xbb\x1c\x15\x44\x20\xdf\x95\x7f\x7b\xb0\xe6\xd8\x83\xaf\xca\x5c\xde\xe6\x32\xd8\xbc\x1b\x63\x3e\x02\x00\x00\xff\xff\x91\x74\x69\x8e\x58\x02\x00\x00")

func mutationGraphqlBytes() ([]byte, error) {
	return bindataRead(
		_mutationGraphql,
		"mutation.graphql",
	)
}

func mutationGraphql() (*asset, error) {
	bytes, err := mutationGraphqlBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "mutation.graphql", size: 600, mode: os.FileMode(420), modTime: time.Unix(1600830485, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _queryGraphql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x74\x91\xc1\x6e\xf2\x30\x10\x84\xef\x7e\x8a\xfd\x95\x0b\xbc\x42\x6e\x11\x7f\x45\x39\x20\xb5\x04\xda\xb3\x89\x27\x89\xd5\xc4\x8e\xd6\x6b\xd2\xa8\xea\xbb\x57\x84\x52\xb5\x24\x5c\xbf\xd9\x19\xef\x7a\x64\xe8\x40\xcf\x11\x3c\xd0\x87\x22\x4a\x28\xaf\x7d\x4f\x35\x9a\xc6\x53\xef\xb9\x31\xd4\x22\x04\x5d\x41\xd1\x85\x2e\x04\xef\x92\x52\x2e\x6c\x5d\xb5\x4c\x69\xfb\x2d\xab\x84\xa8\x82\x1c\x02\x38\x13\x61\x7b\x8c\x82\xc5\x19\x12\x6d\x61\xac\xce\x7d\xe4\x02\x57\xa3\x4a\x46\x65\xa5\xdb\x4e\xdb\xca\xdd\xe0\xac\xcc\xcc\x0c\x0a\x90\x19\xba\x1f\xba\xdb\xd8\xac\xbc\x1b\xbc\xaa\xb5\x73\x68\xa6\xdc\x07\x59\x45\x66\xb8\x62\x98\x15\xb7\xde\xdc\xb1\xbd\xe8\x26\x4e\x37\xd8\x01\xae\xd2\x15\x5a\x38\x79\xb5\xce\xf8\x7e\x32\x92\x5b\x81\x9d\x1c\x7a\xfd\x3c\xb3\xf7\xb1\xa8\x67\xae\x7b\x14\xe9\x76\x28\xc1\x0c\xbe\x91\x1e\x4e\x70\xb2\xc3\x09\xee\xf7\x46\x44\xcb\x94\xfe\x14\xa3\xc6\xaa\xd7\x10\x8a\x01\x4c\x06\xa2\x6d\xa3\xc6\x02\x9f\xd8\x97\xb6\xc1\x41\x8e\x6f\x0b\x75\xce\x4c\x46\x2b\x59\x43\x25\xfb\x96\xd6\x5a\xd0\xeb\x81\x02\xf8\x64\x0b\x8c\x23\xe7\x90\x8d\x49\x69\xe3\xe4\x9f\xfa\x79\xed\xff\x25\xf5\x53\x7d\x05\x00\x00\xff\xff\x10\xaa\x2f\x83\x64\x02\x00\x00")

func queryGraphqlBytes() ([]byte, error) {
	return bindataRead(
		_queryGraphql,
		"query.graphql",
	)
}

func queryGraphql() (*asset, error) {
	bytes, err := queryGraphqlBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "query.graphql", size: 612, mode: os.FileMode(420), modTime: time.Unix(1600830076, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _schemaGraphql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x2a\x4e\xce\x48\xcd\x4d\x54\xa8\xe6\x52\x50\x28\x2c\x4d\x2d\xaa\xb4\x52\x08\x04\x51\x5c\x0a\x0a\xb9\xa5\x25\x89\x25\x99\xf9\x79\x56\x0a\xbe\x50\x16\x57\x2d\x17\x57\x6a\x5e\x69\xae\x42\x70\x7e\x51\x89\x6f\x6a\x49\x46\x7e\x0a\x58\xa3\x63\xb0\x33\x97\x82\x82\x8b\x6b\xb0\x33\x57\x2d\x20\x00\x00\xff\xff\xf0\xa8\x3c\xe1\x50\x00\x00\x00")

func schemaGraphqlBytes() ([]byte, error) {
	return bindataRead(
		_schemaGraphql,
		"schema.graphql",
	)
}

func schemaGraphql() (*asset, error) {
	bytes, err := schemaGraphqlBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "schema.graphql", size: 80, mode: os.FileMode(420), modTime: time.Unix(1600397772, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _typeMessageGraphql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x4c\xca\x41\x0a\xc3\x20\x10\x85\xe1\xfd\x9c\xe2\x81\xb7\xe8\x1d\xba\xa9\x27\x28\xf8\x6a\x85\xa9\xca\x38\x25\x09\x21\x77\x0f\x98\x4d\x96\x3f\xff\x17\x10\xcb\xaf\x2b\xe1\x5b\x27\x3e\xcd\x60\xf4\xbf\xd5\x52\x33\xbe\x54\x6d\x58\x9a\x69\x92\xb9\x9f\x1c\xe3\x9d\x89\x5d\x80\x80\xd7\x84\x4c\x77\x87\xe1\x56\x6a\x16\xc0\xb9\xfa\x03\xf1\xca\x43\xce\x00\x00\x00\xff\xff\xc3\x5b\x1d\xb9\x68\x00\x00\x00")

func typeMessageGraphqlBytes() ([]byte, error) {
	return bindataRead(
		_typeMessageGraphql,
		"type/message.graphql",
	)
}

func typeMessageGraphql() (*asset, error) {
	bytes, err := typeMessageGraphqlBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "type/message.graphql", size: 104, mode: os.FileMode(420), modTime: time.Unix(1600316487, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _typeUserattributeGraphql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x74\x90\xcf\x4a\xc4\x30\x10\xc6\xef\x79\x8a\x81\x7d\x0b\x6f\xa5\x0a\x5e\xf6\xd2\xae\x7a\x8e\xcd\xb7\xd9\x81\x76\x12\xa6\x93\x4a\x11\xdf\x5d\x14\x04\xdb\x74\xaf\xbf\xdf\xfc\xfb\xe6\x44\x2f\x33\x94\x1e\x61\x9e\x47\x67\x6b\xc6\x2f\x68\xcc\x94\xdf\x8b\x81\x3e\x1d\x11\xd1\x19\x81\x7d\x9f\x8a\x0e\x78\xa0\xde\x94\x25\xba\xd3\x8f\x68\xfd\x94\x3d\x47\xd9\xd2\xe6\xda\x84\x9a\xcc\xb0\x1a\x5e\xd6\x8c\x3d\xbd\x37\xb4\xbd\x79\x11\x8c\x15\x4e\xb3\xb5\x45\x15\x32\xac\x47\xee\x9c\xc2\x71\xd3\xab\x1f\x4b\xb5\xbc\x03\x24\xfa\x88\x09\x62\x6f\x2c\x21\x7d\xec\x2b\x7a\x36\xf0\x3e\xdf\xdf\xc3\xc2\x25\x95\xe1\x56\xa7\x7a\x36\xcb\x1d\xae\x50\x85\x6e\xcd\xd3\x02\xb1\x0e\x0b\xe4\xdf\x31\x5f\xee\x3b\x00\x00\xff\xff\x45\xfd\x64\x26\x9a\x01\x00\x00")

func typeUserattributeGraphqlBytes() ([]byte, error) {
	return bindataRead(
		_typeUserattributeGraphql,
		"type/userattribute.graphql",
	)
}

func typeUserattributeGraphql() (*asset, error) {
	bytes, err := typeUserattributeGraphqlBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "type/userattribute.graphql", size: 410, mode: os.FileMode(420), modTime: time.Unix(1600830024, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _typeUserresultGraphql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x24\x8a\x3d\x0e\x82\x40\x10\x85\xfb\x39\xc5\x4b\xb8\x05\xb5\x16\x36\x16\x2a\x07\x58\xe1\x49\x26\xca\xec\x3a\xcc\x12\x89\xf1\xee\x46\x2c\xbf\x9f\x06\xdd\x4c\xc7\x8e\x91\xf4\x21\xb1\x16\x6e\xe2\xcf\x78\x0b\xd0\xa0\x33\x7d\x56\x82\x0b\x2d\x50\x92\x87\xf6\x5a\x92\x05\x74\x10\x40\x87\x16\x07\x0b\xd9\xd6\x8b\xaf\xb9\xfe\x02\x6e\x9e\x27\xec\x5f\x69\xc2\x4c\x5f\xb4\xa7\x00\x35\xae\xf7\x13\xc7\x63\x6e\x71\x0e\x57\x1b\xe5\x23\xdf\x00\x00\x00\xff\xff\x85\xff\x11\x5a\x81\x00\x00\x00")

func typeUserresultGraphqlBytes() ([]byte, error) {
	return bindataRead(
		_typeUserresultGraphql,
		"type/userresult.graphql",
	)
}

func typeUserresultGraphql() (*asset, error) {
	bytes, err := typeUserresultGraphqlBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "type/userresult.graphql", size: 129, mode: os.FileMode(420), modTime: time.Unix(1600316487, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// MustAsset is like Asset but panics when Asset would return an error.
// It simplifies safe initialization of global variables.
func MustAsset(name string) []byte {
	a, err := Asset(name)
	if err != nil {
		panic("asset: Asset(" + name + "): " + err.Error())
	}

	return a
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"mutation.graphql":           mutationGraphql,
	"query.graphql":              queryGraphql,
	"schema.graphql":             schemaGraphql,
	"type/message.graphql":       typeMessageGraphql,
	"type/userattribute.graphql": typeUserattributeGraphql,
	"type/userresult.graphql":    typeUserresultGraphql,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for childName := range node.Children {
		rv = append(rv, childName)
	}
	return rv, nil
}

type bintree struct {
	Func     func() (*asset, error)
	Children map[string]*bintree
}

var _bintree = &bintree{nil, map[string]*bintree{
	"mutation.graphql": &bintree{mutationGraphql, map[string]*bintree{}},
	"query.graphql":    &bintree{queryGraphql, map[string]*bintree{}},
	"schema.graphql":   &bintree{schemaGraphql, map[string]*bintree{}},
	"type": &bintree{nil, map[string]*bintree{
		"message.graphql":       &bintree{typeMessageGraphql, map[string]*bintree{}},
		"userattribute.graphql": &bintree{typeUserattributeGraphql, map[string]*bintree{}},
		"userresult.graphql":    &bintree{typeUserresultGraphql, map[string]*bintree{}},
	}},
}}

// RestoreAsset restores an asset under the given directory
func RestoreAsset(dir, name string) error {
	data, err := Asset(name)
	if err != nil {
		return err
	}
	info, err := AssetInfo(name)
	if err != nil {
		return err
	}
	err = os.MkdirAll(_filePath(dir, filepath.Dir(name)), os.FileMode(0755))
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
	if err != nil {
		return err
	}
	err = os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
	if err != nil {
		return err
	}
	return nil
}

// RestoreAssets restores an asset under the given directory recursively
func RestoreAssets(dir, name string) error {
	children, err := AssetDir(name)
	// File
	if err != nil {
		return RestoreAsset(dir, name)
	}
	// Dir
	for _, child := range children {
		err = RestoreAssets(dir, filepath.Join(name, child))
		if err != nil {
			return err
		}
	}
	return nil
}

func _filePath(dir, name string) string {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	return filepath.Join(append([]string{dir}, strings.Split(cannonicalName, "/")...)...)
}
