package userresult

import (
	"time"
)

// UserResult hold user reference for exam related id
type UserResult struct {
	UserID    int32     `gorm:"unique_index;primary_key;column:UserId"`
	Badge     string    `gorm:"type:varchar(100);column:Badge"`
	UtbkRegNo string    `gorm:"type:varchar(30);column:UtbkRegNo"`
	CreatedAt time.Time `gorm:"column:CreatedDate"`
	UpdatedAt time.Time `gorm:"column:LastUpdated"`
}

// TableName Set table name to custom not plurarized version of struct name
func (UserResult) TableName() string {
	return "user_result"
}
