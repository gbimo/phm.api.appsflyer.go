package hello

// Hello model describe simple model structure
type Hello struct {
	Text string
}
