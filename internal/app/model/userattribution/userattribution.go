package userattribution

type UserAttribution struct {
	AfCostValue          string `json:"af_cost_value"`
	AfCostModel          string `json:"af_cost_model"`
	AfCampaign           string `json:"af_campaign"`
	AfAdset              string `json:"af_adset"`
	AfAd                 string `json:"af_ad"`
	AfChannel            string `json:"af_channel"`
	AfCostCurrency       string `json:"af_cost_currency"`
	AfAdType             string `json:"af_ad_type"`
	AfReengagementWindow string `json:"af_reengagement_window"`
	AfSiteid             string `json:"af_siteid"`
	AttributedTouchType  string `json:"attributed_touch_type"`
	HttpReferrer         string `json:"http_referrer"`
	MediaSource          string `json:"media_source"`
	Campaign             string `json:"campaign"`
	EventRevenue         string `json:"event_revenue"`

}