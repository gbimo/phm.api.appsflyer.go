package persistgql

import (
	"bytes"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/go-redis/redis"
	echo "github.com/labstack/echo/v4"
)

type query struct {
	Variables map[string]interface{} `json:"variables" form:"variables" query:"variables"`
	Query     string                 `json:"query" form:"query" query:"query"`
}

type response struct {
	Error []map[string]interface{} `json:"error"`
	Data  map[string]interface{}   `json:"data"`
}

// PersistedGraphqlMiddleware middleware to parse and register persisted graphql hash
func PersistedGraphqlMiddleware(redisClient *redis.Client) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			var err error

			// hold temporary or modified post body since the original will be removed after the buffer read
			var postBody []byte

			// hold parsed postBody data
			q := new(query)

			if c.Request().Method == "POST" {

				// read body data
				if c.Request().Body != nil {
					postBody, _ = ioutil.ReadAll(c.Request().Body)
				}

				// parse postBody
				json.Unmarshal(postBody, &q)

				// generate hash, check hash existence and register new if not found
				hash := fmt.Sprintf("%x", sha256.Sum256([]byte(q.Query)))
				graphqlQuery, err := redisClient.Get(hash).Result()
				if err != nil || graphqlQuery == "" {
					redisClient.Set(hash, q.Query, 0)
				}
			} else {
				hash := c.Request().URL.Query().Get("hash")
				variables := c.Request().URL.Query().Get("variables")
				graphqlQuery := ""

				if hash != "" {
					graphqlQuery, err = redisClient.Get(hash).Result()
				}

				// check hash validation
				if err != nil || graphqlQuery == "" {
					resErrors := make([]map[string]interface{}, 0)

					resError := make(map[string]interface{})
					resError["message"] = "Invalid query hash"

					return c.JSON(400, &response{
						Error: append(resErrors, resError),
					})
				}

				q.Query = graphqlQuery

				// read and parse variables data
				if variables != "" {
					newVar := make(map[string]interface{})
					json.Unmarshal([]byte(variables), &newVar)

					q.Variables = newVar
				}

				postBody, _ = json.Marshal(q)
			}

			// add new or modified post body
			c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(postBody))

			return next(c)
		}
	}
}
