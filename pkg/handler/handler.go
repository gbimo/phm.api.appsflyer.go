package handler

import (
	"context"
	"net/http"

	echo "github.com/labstack/echo/v4"
)

type key int

const (
	// EchoCtx context identifier for echo
	EchoCtx key = iota
)

// GraphQLHandler handle handler wrapper between go-graphql relay with echo
func GraphQLHandler(h http.Handler) echo.HandlerFunc {
	return func(c echo.Context) error {
		oriReq := c.Request()
		oriCtx := oriReq.Context()
		req := oriReq.WithContext(context.WithValue(oriCtx, EchoCtx, c))
		h.ServeHTTP(c.Response(), req)
		return nil
	}
}
