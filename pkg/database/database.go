package database

import (
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	databases Config
)

// Type is the type of database from a Type* constant
type Type string

const (
	// TypeMySQL is db type for mysql
	TypeMySQL Type = "MySQL"
	// TypeMongo is db type for mongo, still TODO
	TypeMongo Type = "MongoDB"
	// TypePostgreSQL is db type for postgresql, still TODO
	TypePostgreSQL Type = "PostgreSQL"
)

// Config contains the database configurations
type Config struct {
	// Database type
	Type     Type
	Host     string
	Port     int
	Name     string
	User     string
	Password string
}

// Configure save database configuration
func Configure(d Config) {
	// Store the config
	databases = d
}

// GetConnection returns the database connection
func GetConnection() *gorm.DB {
	var (
		connectionString string
		dialect          string
	)

	if databases.Type == TypeMySQL {
		connectionString = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local", databases.User, databases.Password, databases.Host, databases.Port, databases.Name)
		dialect = "mysql"
	}
	connection, err := gorm.Open(dialect, connectionString)
	if err != nil {
		log.Fatal(err)
	}
	return connection
}
