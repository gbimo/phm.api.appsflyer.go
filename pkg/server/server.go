package server

import (
	"fmt"

	echo "github.com/labstack/echo/v4"
)

var (
	config Config
)

// Config hold configuration data
type Config struct {
	Port int
}

// Echo hold echo wrapper
type Echo struct {
	Echo *echo.Echo
}

// Start hold echo start wrapper
func (e Echo) Start() error {
	return e.Echo.Start(fmt.Sprintf(":%d", config.Port))
}

// New generate new echo server
func New(d Config) *Echo {
	// Store the config
	config = d

	return &Echo{echo.New()}
}
