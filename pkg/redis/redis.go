package redis

import (
	"fmt"

	redisClient "github.com/go-redis/redis"
)

var (
	config Config
)

// Config hold configuration data
type Config struct {
	Host string
	Port int
}

// Configure save configuration data
func Configure(d Config) {
	// Store the config
	config = d
}

// GetConnection returns the database connection
func GetConnection() *redisClient.Client {
	return redisClient.NewClient(&redisClient.Options{
		Addr: fmt.Sprintf("%s:%d", config.Host, config.Port),
	})
}
