package main

import (
	"log"
	"net/http"

	"bitbucket.org/phm-eng/pahamify.boilerplate.go/config"
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/internal/app/resolver"
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/internal/app/schema"
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/pkg/database"
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/pkg/handler"
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/pkg/middleware/persistgql"
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/pkg/redis"
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/pkg/server"

	logrus "github.com/sirupsen/logrus"
	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	echo "github.com/labstack/echo/v4"
)

func ping(w http.ResponseWriter, r *http.Request) {
	log.Println("Ping")
	w.Write([]byte("ping"))
}

func main() {
	e := server.New(config.Server)
	database.Configure(config.Database)
	redis.Configure(config.Redis)
	redisClient := redis.GetConnection()

	e.Echo.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "OK")
	})

	graphqlSchema := graphql.MustParseSchema(schema.GetRootSchema(), &resolver.Resolver{})
	e.Echo.POST("/graphql", handler.GraphQLHandler(&relay.Handler{Schema: graphqlSchema}), persistgql.PersistedGraphqlMiddleware(redisClient))
	e.Echo.GET("/graphql", handler.GraphQLHandler(&relay.Handler{Schema: graphqlSchema}), persistgql.PersistedGraphqlMiddleware(redisClient))
	e.Echo.File("/graphiql", "web/graphiql.html")
	logrus.SetLevel(logrus.WarnLevel)
	e.Echo.Logger.Fatal(e.Start())
}
