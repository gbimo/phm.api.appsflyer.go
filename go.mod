module bitbucket.org/phm-eng/pahamify.boilerplate.go

go 1.12

require (
	github.com/go-bindata/go-bindata v3.1.2+incompatible // indirect
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/graph-gophers/graphql-go v0.0.0-20190610161739-8f92f34fc598
	github.com/jinzhu/gorm v1.9.10
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/labstack/echo/v4 v4.1.6
	github.com/sirupsen/logrus v1.2.0
	github.com/unknwon/bra v0.0.0-20200517080246-1e3013ecaff8 // indirect
)
