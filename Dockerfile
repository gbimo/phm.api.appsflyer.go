FROM alpine:3.10
WORKDIR /
RUN apk add --no-cache bash
RUN apk add --update curl
ADD phmapp /
ADD web /web
EXPOSE 8070
CMD ["/phmapp"]