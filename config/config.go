package config

import (
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/pkg/database"
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/pkg/redis"
	"bitbucket.org/phm-eng/pahamify.boilerplate.go/pkg/server"
)

var (
	// Database hold db config
	Database = database.Config{
		Type:     database.TypeMySQL,
		Name:     "pahamify_examreport_v1",
		Host:     "localhost",
		Port:     3306,
		User:     "root",
		Password: "0341",
	}

	// Redis hold redis config
	Redis = redis.Config{
		Host: "localhost",
		Port: 6379,
	}

	// Server hold server config
	Server = server.Config{
		Port: 8070,
	}
)
