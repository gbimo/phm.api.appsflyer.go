# Go parameters
GOCMD=go
DEVCMD=bra
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_NAME=phmapp
BINARY_UNIX=$(BINARY_NAME)_unix

all: test build
build: 
	make bind-static
	$(GOBUILD) -o $(BINARY_NAME) -v cmd/$(BINARY_NAME)/main.go
bind-static:
	$(GOCMD) generate ./internal/app/schema
test: 
	$(GOTEST) -v ./...
clean: 
	$(GOCLEAN) cmd/$(BINARY_NAME)
	rm -f $(BINARY_NAME)
	rm -f $(BINARY_UNIX)
dev:
	$(DEVCMD) run
tool:
	$(GOGET) github.com/Unknwon/bra
	$(GOGET) -u github.com/go-bindata/go-bindata/...
build-linux:
	make bind-static
	CGO_ENABLED=0 GOOS=linux $(GOBUILD) -o $(BINARY_UNIX) -a -installsuffix cgo -v cmd/$(BINARY_NAME)/main.go