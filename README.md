# Project Structure

Project Layout convention used in this project based on https://github.com/golang-standards/project-layout

# Stack

- Web Framework: echo (https://echo.labstack.com)
- ORM: gorm (https://gorm.io)
- Grapql Framework: graphql-go (https://github.com/graph-gophers/graphql-go)

# Make Command

Makefile used for task runner in this project. Following command available:

- `make tool` to install tooling used in the makefile
- `make dep` install project dependency
- `make` or `make all` test then run project
- `make test` run go test
- `make build` build project
- `make clean` clean project builds
- `make run` build project then run
- `make dev` run bra (nodemon alternative) to run project then automaticallny build and run when detect changes in files
